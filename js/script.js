(function ($) {
    Drupal.behaviors.myModuleBehavior = {
        attach: function (context, settings) {

            function animateTemplate(blockName, animationName){
                $(blockName + " .view-content .views-row:nth-child(1)").addClass("wow animated " + animationName).attr("data-wow-delay","0.3s");
                $(blockName + " .view-content .views-row:nth-child(2)").addClass("wow animated " + animationName).attr("data-wow-delay","0.6s");
                $(blockName + " .view-content .views-row:nth-child(3)").addClass("wow animated " + animationName).attr("data-wow-delay","0.9s");
                $(blockName + " .view-content .views-row:nth-child(4)").addClass("wow animated " + animationName).attr("data-wow-delay","0.9s");
            }
           
            /*
            $(".navbar > .container").addClass("wow animated fadeInRight").attr("data-wow-delay","0s");
            $(".slideshow").addClass("wow animated fadeInLeft").attr("data-wow-delay","0s");

            $(".home-dichvu .block-title").addClass("wow animated bounceInUp").attr("data-wow-delay","0s");
            animateTemplate(".home-dichvu", "bounceInUp");

            $(".home-tintuc .block-title").addClass("wow animated bounceInUp").attr("data-wow-delay","0s");
            animateTemplate(".home-tintuc", "fadeIn");

            $(".home-duan .block-title").addClass("wow animated bounceInUp").attr("data-wow-delay","0s");
            animateTemplate(".home-duan", "fadeIn");

            $(".home-gallery .block-title").addClass("wow animated bounceInUp").attr("data-wow-delay","0s");
            animateTemplate(".home-gallery", "flipInY");

            $(".home-doitac .block-title").addClass("wow animated bounceInUp").attr("data-wow-delay","0s");
            animateTemplate(".home-doitac", "flipInY");

            $(".footerinfo").addClass("wow animated fadeInLeft").attr("data-wow-delay","0s");
            $(".bando").addClass("wow animated fadeInRight").attr("data-wow-delay","0s");
            */

            function Resize_Product_Image_Field(){
                var imgHeight = $(".field-slideshow-items img").height();
                $(".field-slideshow-items").css("height",imgHeight + "px");
                var controHeight = $(".field-slideshow-wrapper .cycle-controls").height();
                $(".field-slideshow-wrapper .cycle-controls").css("top", imgHeight / 2 - controHeight / 2 + "px");
            }
            Resize_Product_Image_Field();
            $(window).scroll(function(){Resize_Product_Image_Field()});
            $(window).resize(function(){Resize_Product_Image_Field()});
        }
    };
})(jQuery);